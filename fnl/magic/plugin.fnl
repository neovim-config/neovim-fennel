(module magic.plugin
  {autoload {nvim aniseed.nvim
             a aniseed.core
             util magic.util
             packer packer}})

(defn safe-require-plugin-config [name]
  (let [(ok? val-or-err) (pcall require (.. :magic.plugin. name))]
    (when (not ok?)
      (print (.. "dotfiles error: " val-or-err)))))

(defn- use [...]
  "Iterates through the arguments as pairs and calls packer's use function for
  each of them. Works around Fennel not liking mixed associative and sequential
  tables as well."
  (let [pkgs [...]]
    (packer.startup
      (fn [use]
        (for [i 1 (a.count pkgs) 2]
          (let [name (. pkgs i)
                opts (. pkgs (+ i 1))]
            (-?> (. opts :mod) (safe-require-plugin-config))
            (use (a.assoc opts 1 name))))))))

;; Plugins to be managed by packer.
(use
  :JoosepAlviste/nvim-ts-context-commentstring {}
  ; :LnL7/vim-nix {}
  ; :shaunsingh/nord.nvim {}
  :Olical/aniseed {:tag :v3.21.0}
  :Olical/conjure {:tag :v4.23.0 :mod :conjure}
  :Olical/nvim-local-fennel {:tag :v2.11.0}
  :airblade/vim-gitgutter {:mod :gitgutter}
  :airblade/vim-rooter {}
  :andymass/vim-matchup {}
  :christoomey/vim-tmux-navigator {}
  :clojure-vim/clojure.vim {}
  :clojure-vim/vim-jack-in {}
  :folke/which-key.nvim {}
  :gruvbox-community/gruvbox {}
  :guns/vim-sexp {:mod :sexp}
  :honza/vim-snippets {}
  :iamcco/markdown-preview.nvim { :do "cd app && yarn install"  }
  :jiangmiao/auto-pairs {:mod :auto-pairs}
  :junegunn/fzf {:mod :fzf}
  :junegunn/fzf.vim {}
  :junegunn/gv.vim {}
  :sindrets/diffview.nvim {}
  :kyazdani42/nvim-web-devicons {}
  ; :lambdalisue/suda.vim {}
  :mhinz/vim-startify {}
  :mihaifm/bufstop {}
  :neoclide/coc.nvim {:branch :release :mod :coc}
  :norcalli/nvim-colorizer.lua {:mod :colorizer}
  ; :nvim-lua/plenary.nvim {}
  ; :nvim-lua/popup.nvim {}
  ; :nvim-telescope/telescope.nvim {}
  :karb94/neoscroll.nvim {}
  :nvim-treesitter/nvim-treesitter {:do :TSUpdate}
  :p00f/nvim-ts-rainbow {}
  :radenling/vim-dispatch-neovim {}
  :romgrk/barbar.nvim {}
  :sheerun/vim-polyglot {}
  :stsewd/fzf-checkout.vim {}
  :sudormrfbin/cheatsheet.nvim {}
  :szw/vim-maximizer {}
  :tpope/vim-commentary {}
  :tpope/vim-dispatch {}
  :tpope/vim-eunuch {}
  :tpope/vim-fugitive {:mod :fugitive}
  :tpope/vim-repeat {}
  :tpope/vim-rhubarb {}
  :tpope/vim-sexp-mappings-for-regular-people {}
  :tpope/vim-speeddating {}
  :tpope/vim-surround {}
  :shadmansaleh/lualine.nvim {:mod :lualine}
  :wbthomason/packer.nvim {}
  :windwp/nvim-ts-autotag {}
  ; :TimUntersberger/neogit {}
  ; :dense-analysis/ale {:mod :ale}
  ; :easymotion/vim-easymotion {:mod :easymotion}
  ; :glepnir/galaxyline.nvim {:branch :main}
  ; :hrsh7th/nvim-compe {:mod :compe}
  ; :kassio/neoterm {:mod :neoterm}
  ; :liuchengxu/graphviz.vim {}
  ; :neovim/nvim-lspconfig {}
  ; :nvim-lua/plenary.nvim {:mod :plenary}
  ; :srcery-colors/srcery-vim {:mod :srcery}
  ; :tami5/compe-conjure {}
  ; :tpope/vim-dadbod {}
  ; :tpope/vim-vinegar {}
  ; :vim-test/vim-test {}
  )
