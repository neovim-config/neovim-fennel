(module magic.core
  {autoload {nvim aniseed.nvim}})

;; Generic Neovim configuration.
(set nvim.o.termguicolors true)
(set nvim.o.mouse "a")
(set nvim.o.updatetime 500)
(set nvim.o.timeoutlen 500)
(set nvim.o.sessionoptions "blank,curdir,folds,help,tabpages,winsize")
(set nvim.o.inccommand :split)

(set nvim.g.python3_host_prog "/usr/local/bin/python3")

; ;;; Mappings
; (set nvim.g.mapleader " ")
; (set nvim.g.maplocalleader "ö")

(nvim.ex.set :spell)
(nvim.ex.set :list)

(nvim.ex.set :number)
; (nvim.ex.set :norelativenumber)
(nvim.ex.set :wrap)
(nvim.ex.set "wildmode=full")
; (nvim.ex.set "wildoptions=pum")
(nvim.ex.set "listchars-=eol:↵")

(nvim.ex.set :cursorline)
(nvim.ex.set "diffopt+=vertical")
(nvim.ex.set :expandtab)
(nvim.ex.set :hidden)
(nvim.ex.set :ignorecase)
; (nvim.ex.set :nocompatible)
; (nvim.ex.set :ruler)
(nvim.ex.set "scrolloff=8")
(nvim.ex.set :showmatch)
(nvim.ex.set :smartindent)
(nvim.ex.set "softtabstop=4")
(nvim.ex.set :title)
(nvim.ex.set "colorcolumn=80")

(nvim.ex.set "clipboard+=unnamedplus")

(nvim.ex.colorscheme :gruvbox)

(vim.cmd "source $HOME/.config/nvim/plugin-settings/coc.vim")
(vim.cmd "source $HOME/.config/nvim/plugin-settings/markdown-preview.vim")
(vim.cmd "source $HOME/.config/nvim/plugin-settings/fzf.vim")
; (vim.cmd "luafile $HOME/.config/nvim/plugin-settings/nvim-lsp.vim")
(vim.cmd "luafile $HOME/.config/nvim/plugin-settings/treesitter.lua")
; (nvim.ex.highlight "NormalFloat ctermbg=darkgrey guibg=darkblack")
