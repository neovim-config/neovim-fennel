(module magic.mapping
  {autoload {nvim aniseed.nvim
             nu aniseed.nvim.util
             core aniseed.core}})

(defn- noremap [mode from to]
  "Sets a mapping with {:noremap true}."
  (nvim.set_keymap mode from to {:noremap true}))

;; Generic mapping configuration.
; (nvim.set_keymap :n :<space> :<nop> {:noremap true})
(set nvim.g.mapleader " ")
(set nvim.g.maplocalleader "ö")

; jk escape sequences.
(noremap :i :jk :<esc>)
(noremap :c :jk :<c-c>)
(noremap :t :jk :<c-\><c-n>)

; Spacemacs style leader mappings.
; (noremap :n :<leader>wm ":tab sp<cr>")
; (noremap :n :<leader>wc ":only<cr>")
; (noremap :n :<leader>bd ":bdelete!<cr>")
; (noremap :n :<leader>to ":tabonly<cr>")

; duplicate line
(noremap :n :<A-d> ":t. <CR>==")
(noremap :i :<A-d> "<ESC>:t.<CR>==gi")
(noremap :v :<A-d> ":t$<CR>gv=gv")

; move out of terminal buffers
(noremap :t :<c-w><c-h> "<c-\\><c-n><c-w>h")
(noremap :t :<c-w><c-j> "<c-\\><c-n><c-w>j")
(noremap :t :<c-w><c-k> "<c-\\><c-n><c-w>k")
(noremap :t :<c-w><c-l> "<c-\\><c-n><c-w>l")

;; Delete hidden buffers.
(noremap :n :<leader>bo ":call DeleteHiddenBuffers()<cr>")

;; Correct to first spelling suggestion.
(noremap :n :<leader>zz ":normal! 1z=<cr>")

;; Trim trailing whitespace.
(noremap :n :<leader>bt ":%s/\\s\\+$//e<cr>")

(nu.fn-bridge
  :DeleteHiddenBuffers
  :magic.mapping :delete-hidden-buffers)

(defn delete-hidden-buffers []
  (let [visible-bufs (->> (nvim.fn.range 1 (nvim.fn.tabpagenr :$))
                          (core.map nvim.fn.tabpagebuflist)
                          (unpack)
                          (core.concat))]
    (->> (nvim.fn.range 1 (nvim.fn.bufnr :$))
         (core.filter
           (fn [bufnr]
             (and (nvim.fn.bufexists bufnr)
                  (= -1 (nvim.fn.index visible-bufs bufnr)))))
         (core.run!
           (fn [bufnr]
             (nvim.ex.bwipeout bufnr))))))

(noremap :n :<leader>ec ":e ~/.config/nvim/init.lua<cr>")
(noremap :n :<C-S> ":so %<CR>")
(noremap :n :<leader>ls ":ls<CR>")
(noremap :n :<leader>cd ":cd %:p:h<CR>")

;; Move visual selection vertically
(noremap :x :K ":move '<-2<CR>gv-gv")
(noremap :x :J ":move '>+1<CR>gv-gv")

;; Visual shifting (does not exit Visual mode)
(noremap :v :< "<gv")
(noremap :v :> ">gv")

;; Insert mode shortcut
(noremap :i :<C-h> "<BS>")
(noremap :i :<C-j> "<Down>")
(noremap :i :<C-k> "<Up>")
(noremap :i :<C-b> "<Left>")
(noremap :i :<C-f> "<Right>")

;; Bash like
(noremap :i :<C-a> "<Home>" )
(noremap :i :<C-e> "<End>" )
(noremap :i :<C-d> "<Delete>" )

;; Quit visual mode
(noremap :v :v "<Esc>")
;; Quick command mode
(noremap :n :<CR> ":")

;; Buffer
(noremap :n :<A-.> ":BufferNext<CR>")
(noremap :n :<A-m> ":BufferPrevious<CR>")
; (noremap :n :<leader>. ":BufferPick<CR>")
; (noremap :n :<A-,> ":BufferPrevious<CR>")
(noremap :n :<A-1> ":BufferGoto 1<CR>")
(noremap :n :<A-2> ":BufferGoto 2<CR>")
(noremap :n :<A-3> ":BufferGoto 3<CR>")
(noremap :n :<A-4> ":BufferGoto 4<CR>")
(noremap :n :<A-5> ":BufferGoto 5<CR>")
(noremap :n :<A-6> ":BufferGoto 6<CR>")
(noremap :n :<A-7> ":BufferGoto 7<CR>")
(noremap :n :<A-8> ":BufferGoto 8<CR>")
(noremap :n :<A-9> ":BufferGoto 9<CR>")
(noremap :n :<A-0> ":BufferLast<CR>")
(noremap :n :<leader>da ":BufferCloseAllButCurrent<CR>")
(noremap :n :<leader>dbl ":BufferCloseBuffersLeft<CR>")
(noremap :n :<leader>dbr ":BufferCloseBuffersRight<CR>")
(noremap :n :<leader>bb ":BufferPrevious<CR>")
(noremap :n :<leader>bn ":BufferNext<CR>")
(noremap :n :<leader>bf ":bfirst<CR>")
(noremap :n :<leader>bl ":blast<CR>")
(noremap :n :<leader>bd ":bd<CR>")
(noremap :n :<leader>bc ":BufferClose<CR>")
; (noremap :n :<leader>bk ":bw<CR>")

;; szw/vim-maximizer
(noremap :n :<leader>m ":MaximizerToggle!<CR>")

;;Neoformat
;; nnoremap <leader>F :NeoFormat prettier<CR>

;;bufstop
(noremap :n :<leader>. ":BufstopFast<CR>")

;suda
(noremap :c :w!! "SudaWrite")

;;secure gopass
; au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
