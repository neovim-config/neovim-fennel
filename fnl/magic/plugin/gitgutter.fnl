(module magic.plugin.gitgutter
  {autoload {nvim aniseed.nvim
             util magic.util}})

;; Git-Gutter
;; map <C-g>:GitGutterDisable <BAR> :set laststatus=0 <CR>
(set nvim.g.gitgutter_enabled 1)
(set nvim.g.gitgutter_map_keys 0)

;; nmap <leader>) <Plug>(GitGutterNextHunk)
;; nmap <leader>( <Plug>(GitGutterPrevHunk)
;; nmap <leader>ph <Plug>(GitGutterPreviewHunk)
;; nmap <leader>ghs <Plug>(GitGutterStageHunk)
;; nmap <leader>ghu <Plug>(GitGutterUndoHunk)

