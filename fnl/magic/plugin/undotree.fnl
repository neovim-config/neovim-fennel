; nnoremap <F5> :UndotreeToggle<CR>

; if has("persistent_undo")
;    let target_path = expand('~/.undodir')

;     " create the directory and any parent directories
;     " if the location does not exist.
;     if !isdirectory(target_path)
;         call mkdir(target_path, "p", 0700)
;     endif

;     let &undodir=target_path
;     set undofile
; endif

(module magic.plugin.undotree
  {autoload {nvim aniseed.nvim}})

(nvim.set_keymap
  :n
  :<leader>ut
  ":UndotreeShow<cr>:UndotreeFocus<cr>"
  {:noremap true
   :silent true})
