(module magic.plugin.airline
  {autoload {nvim aniseed.nvim}})

(set nvim.g.airline#extensions#tabline#enabled 1)
(set nvim.g.airline_powerline_fonts 1)
(set nvim.g.airline#extensions#tabline#formatter "unique_tail")
