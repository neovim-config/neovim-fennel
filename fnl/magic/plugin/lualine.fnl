(module magic.plugin.airline
  {autoload {nvim aniseed.nvim
             lualine lualine}})

(lualine.setup
  {:options 
   {:icons_enabled true
    :theme "gruvbox" }})
