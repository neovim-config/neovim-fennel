(module magic.plugin.lsp
  {autoload {nvim aniseed.nvim
             util util}})

;;;lsp config
;;lua require'lspconfig'.tsserver.setup{ on_attach=require'completion'.on_attach }
;;lua require'lspconfig'.pyright.setup{ on_attach=require'completion'.on_attach }
;;lua require'lspconfig'.jdtls.setup{ on_attach=require'completion'.on_attach }

;; nnoremap <silent> gd    <cmd>lua vim.lsp.buf.definition()<CR>
;; nnoremap <silent> gh    <cmd>lua vim.lsp.buf.hover()<CR>
;; nnoremap <silent> gH    <cmd>lua vim.lsp.buf.code_action()<CR>
;; nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
;; nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
;; nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
;; nnoremap <silent> gR    <cmd>lua vim.lsp.buf.rename()<CR>

