(module magic.plugin.neoterm
  {autoload {nvim aniseed.nvim}})

;; kassio/neoterm
(set nvim.g.neoterm_default_mod "vertical")
(set nvim.g.neoterm_size 60)
(set nvim.g.neoterm_autoinsert 1)
(nvim.ex.nnoremap :<c-q> ":Ttoggle<CR>")
(nvim.ex.inoremap :<c-q> "<ESC>:Ttoggle<CR>")
(nvim.ex.tnoremap :<c-q> "<c-\\><c-n>:Ttoggle<CR>")
