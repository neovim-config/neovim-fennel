(module magic.plugin.rainbow
  {autoload {nvim aniseed.nvim}})

;; Rainbow brackets
(set nvim.g.rainbow_active 1)
