(module magic.plugin.fzf
  {autoload {nvim aniseed.nvim
             util magic.util}})

(nvim.ex.command_
  "-bang -nargs=* Rg"
  "call fzf#vim#grep(\""
  "rg --column --line-number --no-heading --color=always --smart-case --hidden --follow -g '!.git/'"
  "-- \".shellescape(<q-args>), 1, fzf#vim#with_preview(), <bang>0)")

(defn- map [from to]
  (util.nnoremap
    from
    (.. ":" to)))

(map :fr "Rg")
(map :fg "GFiles")
(map :* "Rg <c-r><c-w>")
(map :ff "Files")
(map :fb "Buffers")
(map :fw "Windows")
(map :fh "History")
(map :fc "Commands")
(map :fm "Maps")
(map :ft "Filetypes")
(map :fM "Marks")
(map :fH "Helptags")

;; ; fzf.vim
;; ; nmap <C-P> :FZF<CR>
; make sure dir relative to curr file is used for path completion
; (noremap :i :<expr> :<c-x><c-f> """fzf#vim#complete#path(
;      \ "find . -path '*/\.*' -prune -o -print \| sed '1d;s:^..::'",
;      \ fzf#wrap({'dir': expand('%:p:h')}))""")
