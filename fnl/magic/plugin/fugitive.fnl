(module magic.plugin.fugitive
  {autoload {util magic.util}})

(util.nnoremap :gs "Git")
(util.nnoremap :gb "Git blame")
(util.nnoremap :gdi "Git diff")
(util.nnoremap :gpu "Git push")
(util.nnoremap :gpl "Git pull")
(util.nnoremap :gfe "Git fetch")
(util.nnoremap :gc "Git commit --verbose")
(util.nnoremap :gca "Git commit --all --verbose")

; (util.nnoremap :gdl "diffget LOCAL")
; (util.nnoremap :gdr "diffget REMOTE")
(util.nnoremap :dl "diffget //2<CR>")
(util.nnoremap :dr "diffget //3<CR>")
